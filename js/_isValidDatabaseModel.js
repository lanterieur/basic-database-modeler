export function isValidDatabaseModel(value) {
  return value.every(isValidDatabaseModelItem);
}

function isValidDatabaseModelItem(item) {
  if (!item) return false;
  return !!item.title
    ? isValidDatabaseModelEntity(item)
    : isValidDatabaseModelLink(item);
}

function isValidDatabaseModelEntity(e) {
  const areNestedItemsValid = !e.children
    ? true
    : !Array.isArray(e.children)
      ? false
      : e.children.length === 0
        ? true
        : e.children.every(isValidDatabaseModelEntity)
  if (!areNestedItemsValid)
    return false;
  if (!!e.id && !!e.title && typeof e.id === `string` && typeof e.title === `string`) {
    if (!e.children && (!e.type || typeof e.type !== `string`))
      return false;
    return true;
  }
  return false;
}

function isValidDatabaseModelLink(e) {
  if (!!e.from && !!e.to && typeof e.from === `string` && typeof e.to === `string`)
    return true;
  return false;
}