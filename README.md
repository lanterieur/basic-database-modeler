# Basic Database Modeler

A small tool for quick data modelling tasks.

## Install

```bash
git clone https://gitlab.com/lanterieur/basic-database-modeler.git
cd basic-database-modeler
```

## Run

### 1. Run the javascript server

```bash
node server.js
```

### 2. Start a web server in the directory

- You can use the "Live Server" VsCode extension.
- You can create a web server in the programming language of your choice.
- You can use a NGINX or Apache web server

## Interface

Input the data structure as JSON in the editor.

Visualization will update when the data is valid.

![](./screenshot.png)

## Syntax

Sample data is provided as an example.
`data/data.json`

## Conventions

- Required fields are red
- Fields with default data are blue
